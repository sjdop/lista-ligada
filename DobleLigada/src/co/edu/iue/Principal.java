/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.iue;

import co.edu.iue.list.LinkedList;
import javax.swing.JOptionPane;

/**
 *
 * @author juanosorio
 */
public class Principal {
    
    public static void main(String... args)
    {
        LinkedList list = new LinkedList();
        int option, value;
        Object[] options = {
            "Insertar al inicio\n",
            "Insertar al final",
            "Promedio",
            "Imprimir lista",
            "Borrar al inicio",
            "Borrar al final",
            "Cantidad de nodos",
            "Suma par / impar",
            "Borrar elementos mayores",
            "Suma total de nodos",
            "salir"
                
        };
        JOptionPane.showMessageDialog(null, "James gay!");
        do {
            option = JOptionPane.showOptionDialog(null, "Menu de opciones", "Menu", 1, 1, null, options, 5);
            
            switch(option) {
                case 0:
                    value = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el número"));
                    list.insertBefore(value);
                    break;
                case 1:
                    value = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el número"));;
                    list.insertAfter(value);
                    break;
                case 2:
                    JOptionPane.showMessageDialog(null, "El promedio es " + list.getAverage());
                    break;
                case 3:
                    JOptionPane.showMessageDialog(null,list.toString());
                    break;
                case 4:
                    list.deleteBefore();
                    JOptionPane.showMessageDialog(null,"El nodo a sido borrado");
                    break;
                case 5:
                    list.deleteAfter();
                    JOptionPane.showMessageDialog(null,"El nodo a sido borrado");
                    break;
                case 6:
                    JOptionPane.showMessageDialog(null, "La suma de los elementos de la lista es " + list.sumNodes());
                    break;
                case 7:
                    JOptionPane.showMessageDialog(null, "La cantidad de nodos de la lista es " + list.length());
                    break;
                default:
                    System.exit(0);

            }//fin case

        } while ( option < 6 );

    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import co.edu.iue.LinkedList;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author salas
 */
public class LinkedListTest {
    
    public LinkedListTest() {
    }
    
    @Test
    public void insertTest() {
        LinkedList list = new LinkedList();
        list.insert(1, 1);
        list.insert(2, 2);
        list.insert(3, 3);
        list.insert(4, 4);
        list.insert(5, 5);
        
        assertEquals("|1|2|3|4|5|", list.toString());
        assertEquals(3.0f, list.getAverage(), 0.0);
    }
    
}

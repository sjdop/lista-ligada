/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.iue;

/**
 *
 * @author salas
 */
public class LinkedList {
    
    private Node head;
    
    public LinkedList() {
        head = null;
    }
    
    public void insert(int code, int value) {
        Node q = new Node(code, value);
        if (head == null) {
            head = q;
            head.setPrevious(head);
            head.setNext(head);
        } else {
            Node p = head;
            while (p.getNext() != head) {
                p = p.getNext();
            }
            p.setNext(q);
            q.setPrevious(p);
            q.setNext(head);
            head.setPrevious(q);
        }
    }
    
    public float getAverage() {
        if (head == null) {
            return 0;
        }
        Node p = head;
        float sum = 0;
        int count = 0;
        do {
            sum += p.getValue();
            count++;
            p = p.getNext();
        } while(p != head);
        return sum / count;
    }
    
    @Override
    public String toString() {
        Node p = head;
        String result = "";
        if (head != null) {
            result += "|";
            do {
                result += p.getCode()+ "|";
                p = p.getNext();
            } while ( p != head );
        }
        return result;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.iue;

import javax.swing.JOptionPane;

/**
 *
 * @author salas
 */
public class Node {
    
    private String name, address, city;
    private int employees;
    private Node link;
    
    public Node(String name, String address, String city, int employees) {
        this.name = name;
        this.address = address;
        this.city = city;
        this.employees = employees;
        link = null;
    }
    
    public Node() {
        do {
            name = JOptionPane.showInputDialog("Ingrese el nombre del almacén");
        } while (name.trim().equals(""));
        do {
            address = JOptionPane.showInputDialog("Ingrese la dirección del almacén");
        } while (address.trim().equals(""));
        do {
            city = JOptionPane.showInputDialog("Ingrese el nombre de la ciudad");
        } while (city.trim().equals(""));
        do {
            try {
                employees = Integer.parseInt(JOptionPane.showInputDialog("Ingrese la cantidad de empleados"));
                if (employees < 0)
                    JOptionPane.showMessageDialog(null, "Ingrese un numero valido");
            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(null, "Ingrese un numero valido");
                employees = -1;
            }
        } while (employees == -1);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getEmployees() {
        return employees;
    }

    public void setEmployees(int employees) {
        this.employees = employees;
    }

    public Node getLink() {
        return link;
    }

    public void setLink(Node link) {
        this.link = link;
    }
    
    @Override
    public String toString() {
        return "Nombre: " + getName()
                + "\nDirección: " + getAddress()
                + "\nCiudad: " + getCity()
                + "\nEmpleados: " + getEmployees()
                + "\n\n";
    }
    
    @Override
    public Node clone() {
        return new Node(name, address, city, employees);
    }
}

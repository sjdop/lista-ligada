/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.iue;

import javax.swing.JOptionPane;

/**
 *
 * @author salas
 */
public class LinkedList {
    
    private Node head;
    
    public LinkedList() {
        head = null;
    }
    
    public void insertStore(Node q) {
        if (head == null) {
            head = q;
        } else {
            Node p = head;
            while (p.getLink() != null) {
                p = p.getLink();
            }
            p.setLink(q);
        }
    }
    
    public boolean removeFirst() {
        if (head != null) {
            Node p = head;
            head = p.getLink();
            p.setLink(null);
            return true;
        }
        return false;
    }
    
    public boolean removeLast() {
        if (head != null) {
            Node p = head;
            if (p.getLink() == null) {
                head = null;
            } else {
                Node previous = head;
                while (p.getLink() != null) {
                    previous = p;
                    p = p.getLink();
                }
                previous.setLink(null);
                return true;
            }
        }
        return false;
    }
    
    public boolean removeStore(String name) {
        if (head != null) {
            Node p = head;
            Node previous = head;
            while (p != null) {
                if (p.getName()
                        .toLowerCase()
                        .equals( name.toLowerCase() ) ) {
                    if (p == previous) {
                        head = p.getLink();
                        p.setLink(null);
                    } else {
                        previous.setLink(p.getLink());
                        p.setLink(null);
                    }
                    return true;
                }
                previous = p;
                p = p.getLink();
            }
        }
        return false;
    }
    
    public Node search(String name) {
        if (head != null) {
            Node p = head;
            do {
                if (p.getName()
                        .toLowerCase()
                        .equals( name.toLowerCase() ) ) {
                    return p;
                }
            } while((p = p.getLink()) != null);
        }
        return null;
    }
    
    public boolean searchByCity(String city) {
        boolean found = false;
        String result = "Resultados de busqueda por ciudad: " + city + "\n\n";
        if (head != null) {
            Node p = head;
            do {
                if (p.getCity()
                        .toLowerCase()
                        .equals( city.toLowerCase() ) ) {
                    result += p;
                    found = true;
                }
            } while((p = p.getLink()) != null);
        }
        if ( ! found )
            result += "No se encontraron resultados";
        JOptionPane.showMessageDialog(null, result);
        return found;
    }
    
    public int length() {
        int size = 0;
        if (head != null ) {
            Node p = head;
            do {
                size++;
            } while ((p = p.getLink()) != null);
        }
        return size;
    }
    
    public LinkedList[] getByCity() {
        if (head != null) {
            int size = length();
            LinkedList[] listsByCity = new LinkedList[size];
            String[] cities = new String[size];
            Node p = head;
            int position = 0;
            int lastPosition = 0;
            LinkedList current;
            do {
                position = searchCityInVector(cities, p.getCity());
                if (position == -1) {
                    position = lastPosition;
                    listsByCity[position] = new LinkedList();
                    cities[position] = p.getCity();
                    lastPosition++;
                }
                current = listsByCity[position];
                current.insertStore(p.clone());
            } while ((p = p.getLink()) != null);
            return listsByCity;
        }
        return null;
    }
    
    private int searchCityInVector(String[] cities, String city) {
        for (int i = 0; i < cities.length; i++) {
            if (cities[i] != null
                    && cities[i].toLowerCase().equals(city.toLowerCase()))
                return i;
        }
        return -1;
    }
    
    @Override
    public String toString() {
        Node p = head;
        String result = "";
        while ( p != null ) {
            result += p;
            p = p.getLink();
        }
        return result;
    }
    
    public String toStringWithCity() {
        String result = "Ciudad: " + head.getCity() + "\n\n";
        return result + toString();
    }
    
    public String minimize() {
        Node p = head;
        String result = "|";
        while ( p != null ) {
            result += p.getName()+ "|";
            p = p.getLink();
        }
        return result;
    }
    
}

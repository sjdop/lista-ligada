/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.iue.test;

import co.edu.iue.LinkedList;
import co.edu.iue.Node;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author salas
 */
public class LinkedListTest {
    
    public LinkedListTest() {
    }
    
    @Test
    public void testInsertAndSearch() {
        LinkedList list = new LinkedList();
        
        Node n1 = new Node("A", "B", "E", 3);
        Node n2 = new Node("B", "D", "C", 3);
        Node n3 = new Node("C", "E", "D", 3);
        Node n4 = new Node("D", "A", "B", 3);
        Node n5 = new Node("E", "C", "A", 3);
        
        assertEquals(0, list.length());
        list.insertStore(n1);
        assertEquals(1, list.length());
        list.insertStore(n2);
        assertEquals(2, list.length());
        list.insertStore(n3);
        assertEquals(3, list.length());
        list.insertStore(n4);
        assertEquals(4, list.length());
        list.insertStore(n5);
        assertEquals(5, list.length());
        
        assertEquals("|A|B|C|D|E|", list.toString());
        
        assertEquals(n3, list.search("c"));
        assertEquals(n5, list.search("E"));
        assertEquals(n1, list.search("A"));
        assertNull(list.search("R"));
        assertEquals(n1, list.search("a"));
        
        assertTrue(list.searchByCity("E"));
        assertFalse(list.searchByCity(""));
        assertTrue(list.searchByCity("c"));
        assertTrue(list.searchByCity("a"));
    }
    
    @Test
    public void testRemoveFirst() {
        LinkedList list = new LinkedList();
        
        Node n1 = new Node("A", "B", "E", 3);
        Node n2 = new Node("B", "D", "C", 3);
        Node n3 = new Node("C", "E", "D", 3);
        Node n4 = new Node("D", "A", "B", 3);
        Node n5 = new Node("E", "C", "A", 3);
        
        list.insertStore(n1);
        list.insertStore(n2);
        list.insertStore(n3);
        list.insertStore(n4);
        list.insertStore(n5);
        
        list.removeFirst();
        assertEquals("|B|C|D|E|", list.toString());
        list.removeFirst();
        assertEquals("|C|D|E|", list.toString());
        list.removeFirst();
        assertEquals("|D|E|", list.toString());
        list.removeFirst();
        assertEquals("|E|", list.toString());
        list.removeFirst();
        assertEquals("|", list.toString());
        list.removeFirst();
        assertEquals("|", list.toString());
        
        assertFalse(list.removeFirst());
    }
    
    @Test
    public void testRemoveLast() {
        LinkedList list = new LinkedList();
        
        Node n1 = new Node("A", "B", "E", 3);
        Node n2 = new Node("B", "D", "C", 3);
        Node n3 = new Node("C", "E", "D", 3);
        Node n4 = new Node("D", "A", "B", 3);
        Node n5 = new Node("E", "C", "A", 3);
        
        list.insertStore(n1);
        list.insertStore(n2);
        list.insertStore(n3);
        list.insertStore(n4);
        list.insertStore(n5);
        
        list.removeLast();
        assertEquals("|A|B|C|D|", list.toString());
        list.removeLast();
        assertEquals("|A|B|C|", list.toString());
        list.removeLast();
        assertEquals("|A|B|", list.toString());
        list.removeLast();
        assertEquals("|A|", list.toString());
        list.removeLast();
        assertEquals("|", list.toString());
        list.removeLast();
        assertEquals("|", list.toString());
        
        assertFalse(list.removeLast());
    }
    
    @Test
    public void testRemoveStore() {
        LinkedList list = new LinkedList();
        
        Node n1 = new Node("A", "B", "E", 3);
        Node n2 = new Node("B", "D", "C", 3);
        Node n3 = new Node("C", "E", "D", 3);
        Node n4 = new Node("D", "A", "B", 3);
        Node n5 = new Node("E", "C", "A", 3);
        
        assertFalse(list.removeStore("A"));
        list.insertStore(n1);
        list.insertStore(n2);
        list.insertStore(n3);
        list.insertStore(n4);
        list.insertStore(n5);
        
        list.removeStore("C");
        assertEquals("|A|B|D|E|", list.toString());
        
        assertFalse(list.removeStore("C"));
        assertTrue(list.removeStore("A"));
        assertEquals("|B|D|E|", list.toString());
        assertTrue(list.removeStore("B"));
        assertTrue(list.removeStore("E"));
        assertTrue(list.removeStore("D"));
        assertEquals("|", list.toString());
    }
    
    @Test
    public void testSearchByCity() {
        LinkedList list = new LinkedList();
        
        Node n1 = new Node("A", "B", "E", 3);
        Node n2 = new Node("B", "D", "C", 3);
        Node n3 = new Node("C", "E", "D", 3);
        Node n4 = new Node("D", "A", "B", 3);
        Node n5 = new Node("E", "C", "A", 3);
        Node n6 = new Node("F", "B", "E", 3);
        Node n7 = new Node("G", "D", "C", 3);
        Node n8 = new Node("H", "E", "D", 3);
        Node n9 = new Node("I", "A", "B", 3);
        Node n10 = new Node("J", "C", "A", 3);
        
        LinkedList[] searchByCities = list.getByCity();
        assertNull(searchByCities);
        list.insertStore(n1);
        list.insertStore(n2);
        list.insertStore(n3);
        list.insertStore(n4);
        list.insertStore(n5);
        list.insertStore(n6);
        list.insertStore(n7);
        list.insertStore(n8);
        list.insertStore(n9);
        list.insertStore(n10);
        searchByCities = list.getByCity();
        String[] results = {
            "|A|F|",
            "|B|G|",
            "|C|H|",
            "|D|I|",
            "|E|J|"
        };
        LinkedList current;
        for (int i = 0; i < searchByCities.length; i++) {
            current = searchByCities[i];
            if (current != null) {
                assertEquals(results[i], current.toString());
            } else {
                assertNull(current);
            }
        }
    }
    
    @Test
    public void testRemove() {
        LinkedList list = new LinkedList();
        
        Node n1 = new Node("A", "B", "E", 3);
        assertFalse(list.removeStore("a"));
        list.insertStore(n1);
        assertTrue(list.removeStore("a"));
        assertFalse(list.removeStore("z"));
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import co.edu.iue.lista.ListaDobleCircular;
import co.edu.iue.lista.Nodo;

/**
 *
 * @author salas
 */
public class ListaTest {
    
    public ListaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void listTest() {
        ListaDobleCircular lista = new ListaDobleCircular();
        
        assertEquals("", lista.toString());
        lista.eliminar(1);
        lista.insertar(1, "Juan", 2, 20000);
        assertEquals("|1|", lista.toString());
        lista.eliminar(1);
        assertEquals("", lista.toString());
        lista.insertar(1, "Juan", 2, 20000);
        lista.insertar(2, "Juan", 2, 20000);
        assertEquals("|1|2|", lista.toString());
        lista.insertar(3, "Juan", 2, 20000);
        assertEquals("|1|2|3|", lista.toString());
        lista.insertar(4, "Juan", 2, 20000);
        assertEquals("|1|2|3|4|", lista.toString());
        lista.insertar(5, "Juan", 2, 20000);
        assertEquals("|1|2|3|4|5|", lista.toString());
        lista.eliminar(3);
        assertEquals("|1|2|4|5|", lista.toString());
        lista.eliminar(5);
        assertEquals("|1|2|4|", lista.toString());
        lista.eliminar(1);
        assertEquals("|2|4|", lista.toString());
        lista.eliminar(1);
        assertEquals("|2|4|", lista.toString());
        lista.eliminar(4);
        assertEquals("|2|", lista.toString());
        lista.eliminar(2);
        assertEquals("", lista.toString());
    }
    
    @Test
    public void funcionesTest() {
        ListaDobleCircular lista = new ListaDobleCircular();
        lista.insertar(1, "a", 4, 10000);
        lista.insertar(2, "b", 2, 30000);
        lista.insertar(3, "c", 1, 1000);
        lista.insertar(4, "d", 4, 2500);
        lista.insertar(5, "e", 3, 15000);
        lista.insertar(6, "f", 4, 17000);
        lista.insertar(7, "g", 4, 20000);
        lista.insertar(8, "h", 6, 100000);
        lista.insertar(9, "i", 2, 1500);
        lista.insertar(10, "j", 4, 17000);
        lista.insertar(11, "k", 1, 1500);
        lista.insertar(12, "l", 3, 1000);
        lista.insertar(13, "m", 5, 10000);
        
        assertEquals("|1|2|3|4|5|6|7|8|9|10|11|12|13|", lista.toString());
        assertEquals(5, lista.contar_estrato_4());
        assertEquals("h", lista.obtener_valor_mayor());
        assertEquals(17423.0769, lista.promedio_matricula(), 1);
    }
}

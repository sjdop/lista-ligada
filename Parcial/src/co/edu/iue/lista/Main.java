/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.iue.lista;

import javax.swing.JOptionPane;

/**
 *
 * @author salas
 */
public class Main {
    
    public static void main(String... args)
    {
        ListaDobleCircular lista = new ListaDobleCircular();
        int option;
        Object[] options = {
            "Insertar",
            "Eliminar",
            "Estudiantes estrato 4",
            "Nombre estudiante mayor matricula",
            "Promedio matriculas",
            "salir"
        };
        int codigo, estrato;
        String nombre;
        double matricula;
        do {
            option = JOptionPane.showOptionDialog(null, "Menu de opciones", "Menu", 1, 1, null, options, 5);
            
            switch(option) {
                case 0:
                    codigo = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el codigo"));
                    nombre = JOptionPane.showInputDialog("Ingrese el nombre");
                    estrato = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el estrato"));
                    matricula = Double.parseDouble(JOptionPane.showInputDialog("Ingrese el valor de la matricula"));
                    lista.insertar(codigo, nombre, estrato, matricula);
                    break;
                case 1:
                    codigo = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el codigo"));
                    lista.eliminar(codigo);
                    break;
                case 2:
                    JOptionPane.showMessageDialog(null,lista.contar_estrato_4());
                    break;
                case 3:
                    JOptionPane.showMessageDialog(null,lista.obtener_valor_mayor());
                    break;
                case 4:
                    JOptionPane.showMessageDialog(null,lista.promedio_matricula());
                    break;
                default:
                    System.exit(0);

            }//fin case

        } while ( option < 6 );

    }

    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.iue.lista;

/**
 *
 * @author salas
 */
public class Nodo {
    
    private int codigo, estrato;
    private String nombre;
    private double valor_matricula;
    private Nodo ligaIzq, ligaDer;
    
    public Nodo(int codigo, String nombre, int estrato, double valor_matricula)
    {
        this.codigo = codigo;
        this.nombre = nombre;
        this.estrato = estrato;
        this.valor_matricula = valor_matricula;
        ligaIzq = null;
        ligaDer = null;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public int getEstrato() {
        return estrato;
    }

    public void setEstrato(int estrato) {
        this.estrato = estrato;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getValor_matricula() {
        return valor_matricula;
    }

    public void setValor_matricula(double valor_matricula) {
        this.valor_matricula = valor_matricula;
    }

    public Nodo getLigaIzq() {
        return ligaIzq;
    }

    public void setLigaIzq(Nodo ligaIzq) {
        this.ligaIzq = ligaIzq;
    }

    public Nodo getLigaDer() {
        return ligaDer;
    }

    public void setLigaDer(Nodo ligaDer) {
        this.ligaDer = ligaDer;
    }
    
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.iue.lista;

import javax.swing.JOptionPane;

/**
 *
 * @author salas
 */
public class ListaDobleCircular {
    
    private Nodo cabeza;
    
    public ListaDobleCircular() {
        cabeza = null;
    }
    
    public void insertar(int codigo, String nombre, int estrato, double valor_matricula) {
        Nodo q = new Nodo(codigo, nombre, estrato, valor_matricula);
        if (cabeza == null) {
            cabeza = q;
            cabeza.setLigaIzq(cabeza);
            cabeza.setLigaDer(cabeza);
        } else {
            Nodo p = cabeza.getLigaIzq();
            p.setLigaDer(q);
            q.setLigaIzq(p);
            q.setLigaDer(cabeza);
            cabeza.setLigaIzq(q);
        }
    }
    
    public void eliminar(int codigo) {
        if (cabeza != null) {
            Nodo p = cabeza;
            if (cabeza.getLigaDer() != cabeza) {
                do {
                    if (p.getCodigo() == codigo) {
                        if (p == cabeza)
                            cabeza = p.getLigaDer();
                        p.getLigaIzq().setLigaDer(p.getLigaDer());
                        p.getLigaDer().setLigaIzq(p.getLigaIzq());
                        p.setLigaIzq(null);
                        p.setLigaDer(null);
                        return;
                    } 
                    p = p.getLigaDer();
                } while (p != cabeza);
            } else {
                if (p.getCodigo() == codigo) {
                    cabeza = null;
                    return;
                }
            }
            JOptionPane.showMessageDialog(null, "No se encontro");
        } else {
            JOptionPane.showMessageDialog(null, "Lista vacia");
        }
    }
    
    public int contar_estrato_4() {
        int contador = 0;
        if (cabeza != null) {
            Nodo p = cabeza;
            do {
                if (p.getEstrato() == 4)
                    contador++;
                p = p.getLigaDer();
            } while (p != cabeza);
        }
        return contador;
    }
    
    public String obtener_valor_mayor() {
        Nodo mayor = null;
        if (cabeza != null) {
            Nodo p = cabeza;
            do {
                if (mayor == null) {
                    mayor = p;
                } else if (p.getValor_matricula() > mayor.getValor_matricula()){
                    mayor = p;
                }
                p = p.getLigaDer();
            } while (p != cabeza);
        }
        return mayor != null ? mayor.getNombre() : "No hay estudiantes";
    }
    
    public double promedio_matricula() {
        int contador = 0;
        double total = 0;
        if (cabeza != null) {
            Nodo p = cabeza;
            do {
                total += p.getValor_matricula();
                contador++;
                p = p.getLigaDer();
            } while (p != cabeza);
        }
        return contador == 0 ? 0 : total / contador;
    }
    
    @Override
    public String toString() {
        String result = "";
        if (cabeza != null) {
            Nodo p = cabeza;
            result += "|";
            do {
                result += p.getCodigo() + "|";
                p = p.getLigaDer();
            } while(p != cabeza);
        }
        return result;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.iue.list;

import javax.swing.JOptionPane;

/**
 *
 * @author juanosorio
 */
public class LinkedList {
    
    private Node head;
    
    public LinkedList() {
        this.head = null;
    }
    
    public void insertBefore(int data) {
        Node p = new Node(data);
        p.setLink(head);
        head = p;
    }
    
    public void insertAfter(int data) {
        Node q = new Node(data);
        if (head == null) {
            head = q;
        } else {
            Node p = head;
            while (p != null) {
                if (p.getLink() == null) {
                    p.setLink(q);
                    return;
                }
                p = p.getLink();
            }
        }
    }
    
    public void deleteBefore() {
        Node p = head;
        if (p != null)
            head = p.getLink();
    }
    
    public void deleteAfter() {
        Node p = head;
        if (p != null) {
            Node previous = p;
            while (p.getLink() != null) {
                previous = p;
                p = p.getLink();
            }
            if (previous == head)
                head = null;
            else
                previous.setLink(null);
        }
    }
    
    public int sumNodes()
    {
        Node p = head;
        int sum = 0;
        while( p != null) {
            sum += p.getData();
            p = p.getLink();
        }
        return sum;
    }
    
    public int length()
    {
        Node p = head;
        int count = 0;
        while( p != null) {
            count++;
            p = p.getLink();
        }
        return count;
    }
    
    public float getAverage()
    {
        Node p = head;
        float avg = this.sumNodes();
        int nodes = this.length();
        return nodes != 0 ? avg / nodes : nodes;
    }
    
    public boolean buscarValor(int value)
    {
        Node p = head;
        if (head == null) {
            
        } else {
            while (p != null) {
                if (value == p.getData()) {
                    return true;
                }
            }
        }
        return false;
    }
    
    @Override
    public String toString() {
        Node p = head;
        String result = "|";
        while ( p != null ) {
            result += p.getData() + "|";
            p = p.getLink();
        }
        return result;
    }
    
}

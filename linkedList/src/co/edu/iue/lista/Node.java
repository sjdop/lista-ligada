/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.iue.lista;

/**
 *
 * @author juanosorio
 */
public class Node {
    
    private Store data;
    private Node link;

    public Node(Store data) {
        this.data = data;
        link = null;
    }
    
    public Store getData() {
        return data;
    }

    public void setData(Store data) {
        this.data = data;
    }

    public Node getLink() {
        return link;
    }

    public void setLink(Node link) {
        this.link = link;
    }
    
}

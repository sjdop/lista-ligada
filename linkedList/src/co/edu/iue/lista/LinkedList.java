/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.iue.lista;

import java.util.List;
import java.util.ArrayList;

/**
 *
 * @author juanosorio
 */
public class LinkedList implements LinkedListInterface {
    
    private Node head;
    private String city;
    
    public LinkedList() {
        this.head = null;
        this.city = null;
    }
    
    public void setCity(String city) {
        this.city = city;
    }
    
    public String getCity() {
        return city;
    }
    
    @Override
    public void insert(Store store) {
        Node p = new Node(store);
        if (head == null) {
            head = p;
        } else {
            Node q = head;
            while (q != null) {
                if (q.getLink() == null) {
                    q.setLink(p);
                    return;
                }
                q = q.getLink();
            }
        }
    }
    
    public void insertBefore(Store store) {
        Node p = new Node(store);
        p.setLink(head);
        head = p;
    }
    
    public void insertAfter(Store store) {
        Node q = new Node(store);
        Node p = head;
        if (head == null) {
            head = q;
        } else {
            while (p != null) {
                if (p.getLink() == null) {
                    p.setLink(q);
                    return;
                }
                p = p.getLink();
            }
        }
    }
    
    @Override
    public void delete(Store store) {
        Node p = head;
        Node previous = p;
        while (p != null) {
            if (p.getData() == store) {
                if (previous == p) {
                    head = p.getLink();
                } else {
                    previous.setLink(p.getLink());
                }
                return;
            }
            previous = p;
            p = p.getLink();
        }
    }
    
    public void deleteBefore() {
        Node p = head;
        if (p != null)
            head = p.getLink();
    }
    
    public void deleteAfter() {
        Node p = head;
        Node previous = p;
        if (p != null) {
            while (p.getLink() != null) {
                previous = p;
                p = p.getLink();
            }
            previous.setLink(null);
        }
     }

    @Override
    public Store search(String name) {
        Node p = head;
        while (p != null) {
            if ( name.equals( p.getData().getName() ) )
                return p.getData();
            p = p.getLink();
        }
        return null;
    }
    
    @Override
    public String toString() {
        Node p = head;
        String result = "|";
        while ( p != null ) {
            result += p.getData().getName() + "|";
            p = p.getLink();
        }
        return result;
    }
    
    public ArrayList<LinkedList> getStoresByCities() {
        ArrayList<LinkedList> vector = new ArrayList<LinkedList>();
        Node p = head;
        LinkedList current = null;
        Store store;
        while ( p != null ) {
            store = p.getData();
            for (LinkedList list : vector) {
                if ( store.getCity().equals( list.getCity() ) ) {
                    current = list;
                    break;
                }
            }
            if (current == null) {
                current = new LinkedList();
                current.setCity(store.getCity());
                vector.add(current);
            }
            current.insert(store);
            p = p.getLink();
            current = null;
        }
        return vector;
    } 
    
}

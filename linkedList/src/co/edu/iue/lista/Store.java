/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.iue.lista;

import javax.swing.JOptionPane;

/**
 *
 * @author juanosorio
 */
public class Store {
    
    private String name, address, city;
    private int employees;
    
    public Store() {
        name = JOptionPane.showInputDialog("Ingrese el nombre del almacén");
        address = JOptionPane.showInputDialog("Ingrese la dirección del almacén");
        city = JOptionPane.showInputDialog("Ingrese la ciudad del almacén");
        employees = Integer.parseInt(JOptionPane.showInputDialog(
                "Ingrese la cantidad de empleados"
        ));
    }
    
    public Store(String name, String address, String city, int employees) {
        this.name = name;
        this.address = address;
        this.city = city;
        this.employees = employees;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getEmployees() {
        return employees;
    }

    public void setEmployees(int employees) {
        this.employees = employees;
    }
    
    @Override
    public String toString() {
        return "Nombre: " + name
            + "\nDirección: " + address
            + "\nCiudad: " + city
            + "\nEmpleados: " + employees;
    }
    
}

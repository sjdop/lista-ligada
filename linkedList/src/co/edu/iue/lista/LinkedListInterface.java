/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.iue.lista;

/**
 *
 * @author juanosorio
 */
public interface LinkedListInterface {
    
    public void insert(Store store);
    
    public void delete(Store store);
    
    public Store search(String name);
    
}

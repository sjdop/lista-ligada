/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.iue.list;

/**
 *
 * @author salas
 */
public interface ListInterface {
    
    public void insertBefore(int d);
    
    public void insertAfter(int d);
    
    public void deleteBefore();
    
    public void deleteAfter();
    
}

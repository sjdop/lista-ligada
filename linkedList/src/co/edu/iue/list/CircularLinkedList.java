/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.iue.list;

/**
 *
 * @author salas
 */
public class CircularLinkedList implements ListInterface {

    Node head;
    
    public CircularLinkedList() {
        head = null;
    }
    
    @Override
    public void insertBefore(int d) {
        Node p, q;
        q = new Node(d);
        p = head;
        if (head == null) {
            head = q;
            head.setLink(head);
        } else {
            do {
                p = p.getLink();
            } while (p.getLink() != head);
            q.setLink(head);
            p.setLink(q);
            head = q;
        }
    }

    @Override
    public void insertAfter(int d) {
        Node p, q;
        q = new Node(d);
        if (head == null) {
            head = q;
            head.setLink(head);
        } else {
            p = head;
            do {
                p = p.getLink();
            } while (p.getLink() != head);
            q.setLink(head);
            p.setLink(q);
        }
    }

    @Override
    public void deleteBefore() {
        Node p = head;
        if (head != null) {
            if (head.getLink() != head) {
                do {
                    p = p.getLink();
                } while (p.getLink() != head);
                head = head.getLink();
                p.getLink().setLink(null);
                p.setLink(head);
            } else {
                head = null;
            }
        }
    }

    @Override
    public void deleteAfter() {
        Node p = head;
        if (head != null) {
            if (head.getLink() != head) {
                Node ant = p;
                do {
                    ant = p;
                    p = p.getLink();
                } while (p.getLink() != head);
                p.setLink(null);
                ant.setLink(head);
            } else {
                head = null;
            }
        }
    }
    
    @Override
    public String toString() {
        Node p = head;
        if (head != null) {
            String result = "|";
            do {
                result += p.getData() + "|";
                p = p.getLink();
            }while ( p != head );
            return result;
        }
        return "";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.iue;

import co.edu.iue.lista.LinkedList;
import co.edu.iue.lista.Store;
import javax.swing.JOptionPane;

/**
 *
 * @author juanosorio
 */
public class Principal {
    
    public static void main(String... args)
    {
        LinkedList list = new LinkedList();
        int option, value;
        Store store;
        Object[] options = {
            "Insertar al inicio",
            "Insertar al final",
            "Imprimir lista",
            "Borrar al inicio",
            "Borrar al final",
            "salir"
        };
        do {
            option = JOptionPane.showOptionDialog(null, "Menu de opciones", "Menu", 1, 1, null, options, 5);
            
            switch(option) {
                case 0:
                    store = new Store();
                    list.insertBefore(store);
                    break;
                case 1:
                    store = new Store();
                    list.insertAfter(store);
                    break;
                case 2:
                    JOptionPane.showMessageDialog(null,list.toString());
                    break;
                case 3:
                    list.deleteBefore();
                    JOptionPane.showMessageDialog(null,"El nodo a sido borrado");
                    break;
                case 4:
                    list.deleteAfter();
                    JOptionPane.showMessageDialog(null,"El nodo a sido borrado");
                    break;
                default:
                    System.exit(0);

            }//fin case

        } while ( option < 6 );

    }
    
}

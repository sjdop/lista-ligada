/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.iue;

import co.edu.iue.list.CircularLinkedList;
import co.edu.iue.list.Node;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author salas
 */
public class CircularLinkedListTest {
    
    public CircularLinkedListTest() {
    }
    
    @Test
    public void testInsertBefore() {
        CircularLinkedList list = new CircularLinkedList();
        list.deleteBefore();
        list.insertBefore(1);
        list.insertBefore(2);
        list.insertBefore(3);
        list.insertBefore(4);
        list.insertBefore(5);
        
        assertEquals("|5|4|3|2|1|", list.toString());
        
        assertEquals("|5|4|3|2|1|", list.toString());
        list.deleteBefore();
        assertEquals("|4|3|2|1|", list.toString());
        list.deleteBefore();
        assertEquals("|3|2|1|", list.toString());
        list.deleteBefore();
        assertEquals("|2|1|", list.toString());
        list.deleteBefore();
        assertEquals("|1|", list.toString());
        list.deleteBefore();
        assertEquals("", list.toString());
    }
    
    @Test
    public void testInsertAfter() {
        CircularLinkedList list = new CircularLinkedList();
        list.deleteBefore();
        list.insertAfter(1);
        list.insertAfter(2);
        list.insertAfter(3);
        list.insertAfter(4);
        list.insertAfter(5);
        
        assertEquals("|1|2|3|4|5|", list.toString());
        
        assertEquals("|1|2|3|4|5|", list.toString());
        list.deleteAfter();
        assertEquals("|1|2|3|4|", list.toString());
        list.deleteAfter();
        assertEquals("|1|2|3|", list.toString());
        list.deleteAfter();
        assertEquals("|1|2|", list.toString());
        list.deleteAfter();
        assertEquals("|1|", list.toString());
        list.deleteAfter();
        assertEquals("", list.toString());
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.iue.test;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

import co.edu.iue.lista.*;
import java.util.ArrayList;

/**
 *
 * @author juanosorio
 */
public class LinkedListTest {
    
    public LinkedListTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void testInsert() {
        Store s1 = new Store("a", "b", "c", 2);
        Store s2 = new Store("b", "c", "a", 4);
        Store s3 = new Store("c", "a", "b", 6);
        
        LinkedList ll = new LinkedList();
        
        ll.insert(s1);
        ll.insert(s2);
        ll.insert(s3);
        
        assertEquals(ll.toString(), "|a|b|c|");
    }
    
    @Test
    public void testInsertBefore() {
        Store s1 = new Store("a", "b", "c", 2);
        Store s2 = new Store("b", "c", "a", 4);
        Store s3 = new Store("c", "a", "b", 6);
        
        LinkedList ll = new LinkedList();
        
        ll.insertBefore(s1);
        ll.insertBefore(s2);
        ll.insertBefore(s3);
        
        assertEquals(ll.toString(), "|c|b|a|");
    }
    
    @Test
    public void testInsertAfter() {
        Store s1 = new Store("a", "b", "c", 2);
        Store s2 = new Store("b", "c", "a", 4);
        Store s3 = new Store("c", "a", "b", 6);
        
        LinkedList ll = new LinkedList();
        
        ll.insertAfter(s1);
        ll.insertAfter(s2);
        ll.insertAfter(s3);
        
        assertEquals(ll.toString(), "|a|b|c|");
    }
    
    @Test
    public void testDelete() {
        Store s1 = new Store("a", "b", "c", 2);
        Store s2 = new Store("b", "c", "a", 4);
        Store s3 = new Store("c", "a", "b", 6);
        
        LinkedList ll = new LinkedList();
        
        ll.insert(s1);
        ll.insert(s2);
        ll.insert(s3);
        
        ll.delete(s3);
        assertEquals(ll.toString(), "|a|b|");
        ll.delete(s1);
        assertEquals(ll.toString(), "|b|");
    }
    
    @Test
    public void testDeleteBefore() {
        Store s1 = new Store("a", "b", "c", 2);
        Store s2 = new Store("b", "c", "a", 4);
        Store s3 = new Store("c", "a", "b", 6);
        
        LinkedList ll = new LinkedList();
        ll.deleteBefore();
        ll.insert(s1);
        ll.insert(s2);
        ll.insert(s3);
        
        ll.deleteBefore();
        assertEquals(ll.toString(), "|b|c|");
        ll.deleteBefore();
        assertEquals(ll.toString(), "|c|");
    }
    
    @Test
    public void testDeleteAfter() {
        Store s1 = new Store("a", "b", "c", 2);
        Store s2 = new Store("b", "c", "a", 4);
        Store s3 = new Store("c", "a", "b", 6);
        
        LinkedList ll = new LinkedList();
        ll.deleteAfter();
        ll.insert(s1);
        ll.insert(s2);
        ll.insert(s3);
        
        ll.deleteAfter();
        assertEquals(ll.toString(), "|a|b|");
        ll.deleteAfter();
        assertEquals(ll.toString(), "|a|");
    }
    
    @Test
    public void testSearch() {
        Store s1 = new Store("a", "b", "c", 2);
        Store s2 = new Store("b", "c", "a", 4);
        Store s3 = new Store("c", "a", "b", 6);
        
        LinkedList ll = new LinkedList();
        
        ll.insert(s1);
        ll.insert(s2);
        ll.insert(s3);
        
        assertEquals(ll.search("b"), s2);
    }
    
    @Test
    public void testCities() {
        Store s1 = new Store("a", "b", "c", 2);
        Store s2 = new Store("b", "c", "a", 4);
        Store s3 = new Store("c", "a", "b", 6);
        Store s4 = new Store("d", "b", "a", 2);
        Store s5 = new Store("e", "c", "a", 4);
        Store s6 = new Store("f", "a", "b", 6);
        
        LinkedList ll = new LinkedList();
        
        ll.insert(s1);
        ll.insert(s2);
        ll.insert(s3);
        ll.insert(s4);
        ll.insert(s5);
        ll.insert(s6);
        
        ArrayList<LinkedList> cities = ll.getStoresByCities();
        
        assertTrue(cities.size() == 3);
        String[] results = { "|a|", "|b|d|e|", "|c|f|" };
        for ( int i = 0; i < cities.size(); i++ ) {
            assertTrue("Lista " + (i + 1) + " contiene datos erroneos", results[i].equals(cities.get(i).toString()));
        }
    }
    
}

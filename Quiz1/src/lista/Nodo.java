/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lista;

/**
 *
 * @author Sebastian Zapata Ocampo
 * @author Juan David Osorio Posada
 */
public class Nodo {
    
    private Nodo ligaIzq, ligaDer;
    private int valor;
    
    public Nodo(int v) {
        valor = v;
    }

    public Nodo obtenerLigaIzq() {
        return ligaIzq;
    }

    public void asignarLigaIzq(Nodo ligaIzq) {
        this.ligaIzq = ligaIzq;
    }

    public Nodo obtenerLigaDer() {
        return ligaDer;
    }

    public void asignarLigaDer(Nodo ligaDer) {
        this.ligaDer = ligaDer;
    }

    public int obtenerValor() {
        return valor;
    }

    public void asignarValor(int valor) {
        this.valor = valor;
    }
    
}

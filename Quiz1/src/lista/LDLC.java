/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lista;

import javax.swing.JOptionPane;

/**
 *
 * @author Sebastian Zapata Ocampo
 * @author Juan David Osorio Posada
 */
public class LDLC {
    
    private Nodo cabeza;
    
    public LDLC() {
        cabeza = null;
    }
    
    public void insertar(int v) {
        Nodo p = new Nodo(v);
        if (cabeza == null) {
            cabeza = p;
            cabeza.asignarLigaIzq(cabeza);
            cabeza.asignarLigaDer(cabeza);
        } else {
            Nodo q = cabeza;
            while (q.obtenerLigaDer() != cabeza) {
                q = q.obtenerLigaDer();
            }
            q.asignarLigaDer(p);
            cabeza.asignarLigaIzq(p);
            p.asignarLigaIzq(q);
            p.asignarLigaDer(cabeza);
        }
    }
    
    public void eliminarAntepenultimo() {
        Nodo p = cabeza;
        int contador = 0;
        if (cabeza == null) {
            JOptionPane.showMessageDialog(null, "No hay antepenultimo");
            return;
        }
        while (contador < 3) {
            p = p.obtenerLigaIzq();
            contador++;
        }
        if (p == cabeza) {
            cabeza = p.obtenerLigaDer();
        }
        if (cabeza.obtenerLigaDer() == cabeza)
            cabeza = null;
        else {
            p.obtenerLigaIzq().asignarLigaDer(p.obtenerLigaDer());
            p.obtenerLigaDer().asignarLigaIzq(p.obtenerLigaIzq());
            p.asignarLigaIzq(null);
            p.asignarLigaDer(null);
        }
        JOptionPane.showMessageDialog(null, "Nodo eliminado");
    }
    
    public void intercambiarNodos(int posicion) {
        Nodo p = cabeza;
        Nodo ultimo, auxiliar;
        if (posicion >= contar()) {
            JOptionPane.showMessageDialog(null, "La posicion es mayor a la cantidad de nodos");
            return;
        }
        int contador = 0;
        if (cabeza != null) {
            while (contador < posicion) {
                p = p.obtenerLigaDer();
                contador++;
            }
            ultimo = cabeza.obtenerLigaIzq();
            if (p == cabeza) {
                cabeza.asignarLigaIzq(ultimo.obtenerLigaIzq());
                ultimo.asignarLigaDer(cabeza.obtenerLigaDer());
                ultimo.obtenerLigaIzq().asignarLigaDer(cabeza);
                ultimo.asignarLigaIzq(cabeza);
                cabeza.obtenerLigaDer().asignarLigaIzq(ultimo);
                cabeza.asignarLigaDer(ultimo);
                cabeza = ultimo;
            } else if (p == ultimo.obtenerLigaIzq()) {
                p.obtenerLigaIzq().asignarLigaDer(ultimo);
                cabeza.asignarLigaIzq(p);
                ultimo.asignarLigaIzq(p.obtenerLigaIzq());
                ultimo.asignarLigaDer(p);
                p.asignarLigaIzq(ultimo);
                p.asignarLigaDer(cabeza);
            }else {
                auxiliar = p.obtenerLigaIzq();
                auxiliar.asignarLigaDer(ultimo);
                p.asignarLigaIzq(ultimo.obtenerLigaIzq());
                p.obtenerLigaDer().asignarLigaIzq(ultimo);
                ultimo.obtenerLigaIzq().asignarLigaDer(p);
                ultimo.asignarLigaDer(p.obtenerLigaDer());
                ultimo.asignarLigaIzq(auxiliar);
                p.asignarLigaDer(cabeza);
                cabeza.asignarLigaIzq(p);
            }
            JOptionPane.showMessageDialog(null, "Nodos intercambiados");
        } else {
            JOptionPane.showMessageDialog(null, "No hay nodos");
        }
    }
    
    public void buscar4InsertarAnterior(int v) {
        Nodo q = new Nodo(v);
        Nodo p = cabeza;
        do {
            p = p.obtenerLigaDer();
            if (p.obtenerValor() == 4) {
                q.asignarLigaIzq(p.obtenerLigaIzq());
                q.asignarLigaDer(p);
                p.obtenerLigaIzq().asignarLigaDer(q);
                p.asignarLigaIzq(q);
                if (p == cabeza)
                    cabeza = q;
                JOptionPane.showMessageDialog(null, "Nodo insertado");
                return;
            }
        } while (p != cabeza);
        JOptionPane.showMessageDialog(null, "No hay nodo con el número 4");
    }
    
    public int contar() {
        Nodo p = cabeza;
        int contar = 0;
        if (p != null) {
            do {
                contar++;
                p = p.obtenerLigaDer();
            } while (p != cabeza);
        }
        return contar;
    }
    
    public String imprimir() {
        String result = "";
        int i = 1;
        Nodo p = cabeza;
        if (p != null) {
            do {
                result += "Nodo " + i + ": " + p.obtenerValor() + "\n";
                p = p.obtenerLigaDer();
                i++;
            } while (p != cabeza);
        } else {
            result = "No hay nodos en la lista";
        }
        return result;
    }
    
}

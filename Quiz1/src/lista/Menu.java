/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lista;

import javax.swing.JOptionPane;

/**
 *
 * @author salas
 */
public class Menu {
    
    public static void main(String... args) {
        LDLC lista = new LDLC();
        int opcion = 0;
        int valor;
        for (int i = 0; i < 3; i++) {
            valor = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el valor del nodo número " + (i + 1)));
            lista.insertar(valor);
        }
        JOptionPane.showMessageDialog(null, "Seleccione la acción que desea realizar");
        do {
            opcion = Integer.parseInt(JOptionPane.showInputDialog(
                    "1. Insertar nuevo nodo\n"
                  + "2. Eliminar el antepenultimo nodo\n"
                  + "3. Ingresar posicion del nodo a intercambiar por el ultimo nodo\n"
                  + "4. Anteponer nodo al que tenga el valor \"4\"\n"
                  + "5. Imprimir lista\n"
                  + "6. Salir"));
            switch (opcion) {
                case 1:
                    valor = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el valor del nuevo nodo"));
                    lista.insertar(valor);
                    break;
                case 2:
                    lista.eliminarAntepenultimo();
                    break;
                case 3:
                    valor = Integer.parseInt(JOptionPane.showInputDialog("Ingrese la posicion del nodo que desea intercambiar por el ultimo"));
                    lista.intercambiarNodos(valor);
                    break;
                case 4:
                    valor = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el valor del nuevo nodo"));
                    lista.buscar4InsertarAnterior(valor);
                    break;
                case 5:
                    JOptionPane.showMessageDialog(null, lista.imprimir());
                    break;
            }
        } while (opcion != 6);
    }
    
}
